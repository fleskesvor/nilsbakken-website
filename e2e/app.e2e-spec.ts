import { NilsbakkenWebsitePage } from './app.po';

describe('nilsbakken-website App', function() {
  let page: NilsbakkenWebsitePage;

  beforeEach(() => {
    page = new NilsbakkenWebsitePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
